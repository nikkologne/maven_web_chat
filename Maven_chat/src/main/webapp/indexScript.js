/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Slideshow {
    constructor() {

        this.chats = document.querySelector("#chats"); //lag en global variabel chats
        this.debug = false;

        this.load(); //start load
    }
    //henter chat objekter fra databasen
    load() {

        fetch('api/chat/all') //hent fra URL ( trenger bare biten etter domene)
                .then(response => {
                    if (response.ok) {


                        return response.json();

                    }

                    throw new Error("Failed to load list of images");
                })
                .then(json => this.addImages(json)) // start add images og sett inn json arrayet som ble hentet fra databasen inn som input.
                .catch(e => console.log("Error: " + e.message));
    }

    addImages(json) {


        this.chats.innerHTML = '';
        for (let i = 0; i < json.length; i++) {

            let chat = document.createElement('p');
            chat.innerHTML = "chat nr: " + json[i].id;
            let a = document.createElement('a'); //debugging console log. fjern i full verson eller lag debugging if
            a.href = "chat.html?id=" + json[i].id;
            a.appendChild(chat);



            this.chats.appendChild(a);
        }
    }
}
let slideshow = new Slideshow();