/* 
 * Script som henter beskjeder fra fre database og skriver dem ut
 * og sender nye baskjeder til databasen
 */
// classe som henter beskjeder fra database

class PhotoForum {

    constructor() {


        this.message = document.querySelector("#messagetext");
        this.uname = document.querySelector("#name");
        this.chat = new URL(document.URL).searchParams.get("id");



        this.message.onchange = event => {
            console.log(new Messages(event.target.value, this.uname, this.chat, this.chat)); // skriv ut hva som blir sendt
            fetch('api/chat/newMessage',
                    {
                        method: 'POST',
                        body: JSON.stringify(new Messages(event.target.value, this.uname.value, this.chat, this.chat)),
                        headers: {'Content-Type': 'application/json; charset=UTF-8'}
                    })
                    .then(response => {

                        if (response.ok) {
                            console.log("Got");
                            return response.json();
                        }

                        throw new Error("Failed to send message " + event.target.value);
                    })
                    .then(message => {
                        this.message.value = "";
                    })
                    .catch(exception => console.log("Error: " + exception));
        };
    }
}

class Messages {
    constructor(message, name, chatid, chat) {
        this.name = name;
        this.message = message;
        this.version = new Date();
        this.cid = chatid;
        this.chat = new Chat(chat);
        this.messageId;
    }
}

class Chat {
    constructor(id) {
        this.id = id;
    }
}


let forum = new PhotoForum();
class Slideshow {

    constructor() {
        this.chat = document.querySelector("#messagesBoard");
        this.chatid = new URL(document.URL).searchParams.get("id");
       setTimeout("getMessages()",1000);
        this.load(this.chatid);

    }

    load(chatid) {
        console.log("load rann"); //debug console log
        fetch('api/chat/messages')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    }

                    throw new Error("Failed to load list of images");
                })
                .then(json => this.addImages(json, this.chatid))
                .catch(e => console.log("Error: " + e.message));

    }

    addImages(json, chatid) {
        this.chat.innerHTML = '';
        
        for (let i = 0; i < json.length; i++) {
            

            if (json[i].cid == chatid) {
                
                let message = document.createElement('p');
                message.innerHTML = json[i].name + "    -   " + json[i].message;


                this.chat.appendChild(message);
            }


        }
    }
}
let slideshow = new Slideshow();

function getMessages() {
   let slideshow = new Slideshow();
}
getMessages();