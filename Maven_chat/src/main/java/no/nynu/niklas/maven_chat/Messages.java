/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.nynu.niklas.maven_chat;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author niklas
 */
@Data @NoArgsConstructor
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@Entity

public class Messages implements Serializable{
    @Id @GeneratedValue
    long messageId;
    String message;
    String name;
    Long cid;

    @Version
    Timestamp version;
    
    @ManyToOne(optional = false,cascade = CascadeType.PERSIST)
    Users sender;
    
    @XmlTransient
    @ManyToOne(optional = false,cascade = CascadeType.PERSIST)
    Chat chat;
    
    
    
}
