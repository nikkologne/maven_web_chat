package no.nynu.niklas.maven_chat;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author niklas
 * 
 * Queryene er laget av javax biblioteket og databasekomunikasjonen er sottet opp på payara serveren og referert til
 * i persistance.xml filen
 */

@Path("chat") //
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class ChatService {
    @PersistenceContext
    EntityManager em; // EntityManager hondterer database GET og POST
    
    
    @GET
    public Response sayHello(@QueryParam("chat") String chat) {
        
        Chat c = new Chat(); // Nytt chat objekt
        c.setChat(chat); // henter parametere fra link
        em.persist(c); // setter objekt info inn i database
        return Response.ok(c).build(); // returnerer informasjon fra database
        
    }
    
    @POST
    @Path("make")
    public void makeChat(@QueryParam("chat") String name) {
        
        Chat c = new Chat(); // Nytt chat objekt
        c.setChat(name); // henter parametere fra link
        em.persist(c); // setter objekt info inn i database
       // return Response.ok(c).build(); // returnerer informasjon fra database
      
    }
    
    @GET
    @Path("all")
    public List<Chat> getAll() {
        return em.createQuery("Select c from Chat c",Chat.class).getResultList(); //Lag en liste med JSON strenger som inneholder data fra databasen
    }
    @GET
    @Path("chat")
    public Chat getChat(@QueryParam("id") long chatid) {
        return em.find(Chat.class,chatid);
        //return em.createQuery("Select c from Chat c where c.id = :id",Chat.class).setParameter("id", chatid).getSingleResult(); //Lag en liste med JSON strenger som inneholder data fra databasen
    }
    @GET
    @Path("messages")
    public List<Messages> getMessages(){
        return em.createQuery("Select m from Messages m",Messages.class).getResultList(); //Lag en liste med JSON strenger som inneholder data fra databasen
    }
    
    @POST
    @Path("newMessage")
    public Response newMessage(@QueryParam("chatid") long chatid, Messages message) {
        System.out.println(message);
        Chat chat = em.find(Chat.class, chatid);
        System.out.println(chat);
        
        
        
        // check for null, insert if ..
        message.setChat(chat);
        em.persist(message); // setter objekt info inn i database
        return Response.ok(message).build(); // returnerer informasjon fra database
    }
    
    @GET
    @Path("user")
    public List<Users> getUser() {
        return em.createQuery("Select u from Users u",Users.class).getResultList(); //Lag en liste med JSON strenger som inneholder data fra databasen
    }
    @POST
    @Path("newUser")
    public Response newUser(@QueryParam("name") String name) {
        Users user = new Users();
        user.setName(name);
        em.persist(user); // setter objekt info inn i database
        return Response.ok(user).build(); // returnerer informasjon fra database
    }
    @GET
    @Path("text")
    public String getText() {
        return "Testn 123"; //String som skal returneres
    }
}
