package no.nynu.niklas.maven_chat;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author niklas
 */
@Data @NoArgsConstructor
@XmlRootElement
@Entity
public class Chat implements Serializable {
    @Id @GeneratedValue
    long id;
    String chat;
    
    @XmlTransient
    @OneToMany(mappedBy = "chat",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    List<Messages> messages;
}
