
package no.nynu.niklas.maven_chat;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author niklas
 */
@Data @NoArgsConstructor
@XmlRootElement
@Entity
public class Users implements Serializable{
    
    @Id @GeneratedValue 
    
    long userId;
    String name;
    @XmlTransient
    String password;
    
    
}
