/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.nynu.niklas.maven_chat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author niklas
 */
public class Main {
    public static void main(String[] args){
        
        Connection myconObj=null;
        Statement mystatObj=null;
        ResultSet myresObj=null;
        String query = "Select * from ChatDB.USERS";
        
        try {
            Class.forName("org.apache.derby.jdbc.AutoloadedDriver");
            myconObj = DriverManager.getConnection("jdbc:derby://localhost:1527/ChatDB", "root", "sundsjoe4");
            mystatObj = myconObj.createStatement();
            myresObj = mystatObj.executeQuery(query);            
            while (myresObj.next()) {
                int id = myresObj.getInt("USER_ID");
                String name = myresObj.getString("NAME");
                System.out.println("name: " + name + "  id: " + id);
            }
        } catch (SQLException a) {
            a.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
